# Introduction 
This is a simple service to convert HTML to PDF.

It is built by **Node.Js** and based on **Express**, **jsReport** and **puppeteer**.

# Getting Started


# Build and run
## Run

1. Run  
   
    > `npm install | npm start`

2. Send request by using any tools e.g. Postman or curl
       
    > Sample request:  
    > URL: localhost:10001/pdf  
    > Method: POST  
    > `Body: 
    > {
    >	"Content": "<div>Hello World!!!!!</div>",
    >	"FileName": "Test.pdf"
    > }`

## Docker Run

1. Run following script to build the solution to create a docker image (Do not forget the ".")  
   
    > `docker build -t htmltopdf:v1 .`

2. Start a container from the image
   
    > `docker run -d -p 10001:10001 htmltopdf:v1`

3. Send request by using any tools e.g. Postman or curl
       
    > Sample request:  
    > URL: localhost:10001/pdf  
    > Method: POST  
    > `Body: 
    > {
    >	"Content": "<div>Hello World!!!!!</div>",
    >	"FileName": "Test.pdf"
    > }`