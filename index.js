const config = require('./jsreport.config');
const express = require('express');
const app = express();
const bodyParser = require("body-parser");

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
  extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

const server = app.listen(10001);

const jsreport = require('jsreport-core')(config);

jsreport.init().then(() => {
  app.post('/pdf', (req, res) => {
    jsreport.render({
      template: {
        content: req.body.Content,
        engine: 'handlebars',
        recipe: 'chrome-pdf',
      }
    }).then((out) => {
      setResponseHeaders(res, req.body.FileName);
      out.stream.pipe(res);
    }).catch((e) => {
      res.end(e.message);
    });
  })
});

function setResponseHeaders(res, filename) {
  res.header('Content-Disposition', 'inline; filename=' + filename);
  res.header('Content-Type', 'application/pdf');
}