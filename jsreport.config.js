const config = {
  extensions: {
    'chrome-pdf': {
      "launchOptions": {
        "args": ["--no-sandbox"]
      }
    }
  },
  templatingEngines: {
    strategy: 'in-process'
  }
}

module.exports = config;